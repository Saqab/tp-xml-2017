package org.mukhtar.handler.xml;

import org.xml.sax.Attributes;
import org.xml.sax.helpers.DefaultHandler;

public class Handlers1 extends DefaultHandler {

	private int count = 0;
	private static String rootElementName = null;

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	@Override
	public void startDocument() {
		count = 0;
	}

	@Override
	public void startElement(String uri, String localName, String qName, Attributes att) {

		count++;
		if (count == 1)
			rootElementName = localName;
	}

	public String getRootElementName() {
		return rootElementName;
	}

}
