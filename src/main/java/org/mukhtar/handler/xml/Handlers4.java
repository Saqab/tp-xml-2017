package org.mukhtar.handler.xml;

import java.util.ArrayList;
import java.util.List;

import org.xml.sax.Attributes;
import org.xml.sax.helpers.DefaultHandler;

public class Handlers4 extends DefaultHandler {

	private List<String> listeItems = new ArrayList<String>();
	private List<String> listeParents = new ArrayList<String>();
	private int depth = 0;

	public List<String> getListeAttributs() {
		return listeItems;
	}

	@Override
	public void startDocument() {

	}

	@Override
	public void startElement(String uri, String localName, String qName, Attributes att) {

		/* On donne la premiere valeur racine à la listeParent */
		if(depth == 0) {
			listeParents.add(qName);
			depth++;
		}
		
		/* Si l'élement a pour parent "item", alors on ajoute cet element à listeItems */
		if(listeParents.get(listeParents.size()-1).equals("item")) {
			listeItems.add(qName);	
			depth++;
		}


	

	}

	@Override
	public void endElement(String uri, String localName, String qName) {
		/* Le nouveau sous element devient le parent */
		if (listeParents.get(listeParents.size()-1) != qName) {
			listeParents.add(qName);
}		
		

	}
}
