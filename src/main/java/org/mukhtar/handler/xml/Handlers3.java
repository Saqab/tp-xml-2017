package org.mukhtar.handler.xml;

import org.xml.sax.Attributes;
import org.xml.sax.helpers.DefaultHandler;

public class Handlers3 extends DefaultHandler {

	private int count = 0;
	private String nomRacine;
	private int nbElements = 0;
	@Override
	public void startDocument() {
		
	}

	@Override
	public void startElement(String uri, String localName, String qName, Attributes att) {
		if(nbElements == 0)
			nomRacine = qName;
		if(localName.equals("item"))
				count++;
		nbElements++;
	}

	@Override
	public void endElement(String uri, String localName, String qName) {
		if(nomRacine.equals(localName))
			System.out.println("Il y a "+count+" éléments 'item' dans le flux");
	}
}
