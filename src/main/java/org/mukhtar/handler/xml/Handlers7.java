package org.mukhtar.handler.xml;

import java.util.ArrayList;
import java.util.List;

import org.xml.sax.Attributes;
import org.xml.sax.helpers.DefaultHandler;

public class Handlers7 extends DefaultHandler {

	private List<String> listeName = new ArrayList<String>();

	public List<String> getListeNamespace() {
		return listeName;
	}

	@Override
	public void startDocument() {

	}

	@Override
	public void startElement(String uri, String localName, String qName, Attributes att) {
		
		if(att.toString().contains("microservices"));
			listeName.add(localName);
	}

	@Override
	public void endElement(String uri, String localName, String qName) {
	}
}
