package org.mukhtar.handler.xml;

import java.util.ArrayList;
import java.util.List;

import org.xml.sax.Attributes;
import org.xml.sax.helpers.DefaultHandler;

public class Handlers5 extends DefaultHandler {

	private List<String> listeNamespace = new ArrayList<String>();

	public List<String> getListeNamespace() {
		return listeNamespace;
	}

	@Override
	public void startDocument() {

	}

	@Override
	public void startElement(String uri, String localName, String qName, Attributes att) {

		if(listeNamespace.contains(uri) == false)
			listeNamespace.add(uri);

	}

	@Override
	public void endElement(String uri, String localName, String qName) {
	}
}
