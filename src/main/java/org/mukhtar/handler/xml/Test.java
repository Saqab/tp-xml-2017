/* MUKHTAR Saqab */
package org.mukhtar.handler.xml;

import java.io.IOException;
import java.io.InputStream;
import java.util.ListIterator;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.mukhtar.xml.RSSReader;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

/* --- Donne le nom de la racine du fichier XML --- */

public class Test extends DefaultHandler {

	public static void main(String[] args)
			throws ParserConfigurationException, SAXException, UnsupportedOperationException, IOException {

		/* --- Premier test --- */
		SAXParserFactory spf = SAXParserFactory.newInstance();
		spf.setValidating(true);
		spf.setNamespaceAware(true);
		InputStream is = RSSReader.read("https://www.voxxed.com/feed/");
		Handlers1 handler1 = new Handlers1();
		SAXParser p = spf.newSAXParser();
		p.parse(is, handler1);

		System.out.print("Le nom de la racine est : ");
		System.out.println(handler1.getRootElementName());
		System.out.println("------------------------------------------------------------");

		/* --- Deuxième test --- */
		/* On remet à jour le contenu à parser */
		is = RSSReader.read("https://www.voxxed.com/feed/");	
		Handlers2 handler2 = new Handlers2();
		p.parse(is, handler2);
		
		System.out.println("------------------------------------------------------------");
		
		/* --- Troisième test --- */
		is = RSSReader.read("https://www.voxxed.com/feed/");	
		Handlers3 handler3 = new Handlers3();
		p.parse(is, handler3);
		System.out.println("------------------------------------------------------------");	
		
		/* --- Quatrième test --- */
		is = RSSReader.read("https://www.voxxed.com/feed/");	
		Handlers4 handler4 = new Handlers4();
		p.parse(is, handler4);
		
		ListIterator<String> iterateur = handler4.getListeAttributs().listIterator();
		while(iterateur.hasNext())
			System.out.println(iterateur.next());
		System.out.println("Je n'ai malheuresement pas réussi à afficher tous les elements de 'item'");
		System.out.println("------------------------------------------------------------");
		
		/* --- Cinquième test --- */
		is = RSSReader.read("https://www.voxxed.com/feed/");	
		Handlers5 handler5 = new Handlers5();
		p.parse(is, handler5);
		
		ListIterator<String> iterateur2 = handler5.getListeNamespace().listIterator();
		System.out.println("Voici tout les namespaces : ");
		while(iterateur2.hasNext())
			System.out.println(iterateur2.next());
		System.out.println("------------------------------------------------------------");
		
		/* --- Sixième test --- */
		is = RSSReader.read("https://www.voxxed.com/feed/");	
		Handlers6 handler6 = new Handlers6();
		p.parse(is, handler6);
		
		System.out.println("Il y a "+handler6.getCompteur()+
				" elements contenant l'espace de nom http://purl.org/rss/1.0/modules/slash/");
		System.out.println("------------------------------------------------------------");
		
		/* --- Septième test --- */
		is = RSSReader.read("https://www.voxxed.com/feed/");	
		Handlers7 handler7 = new Handlers7();
		p.parse(is, handler7);
		ListIterator<String> iterateur3 = handler7.getListeNamespace().listIterator();
		while(iterateur3.hasNext())
			System.out.println(iterateur3.next());
		System.out.println("Il y a "+handler7.getListeNamespace().size()+" elements contenant 'microservices'");
		System.out.println("Je n'ai pas réussi à trouver les bons elements contenant 'microservices'");	
		System.out.println("------------------------------------------------------------");

		
	}
}
