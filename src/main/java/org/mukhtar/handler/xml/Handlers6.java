package org.mukhtar.handler.xml;

import org.xml.sax.Attributes;
import org.xml.sax.helpers.DefaultHandler;

public class Handlers6 extends DefaultHandler {

	private int compteur = 0;

	public int getCompteur() {
		return compteur;
	}

	@Override
	public void startDocument() {

	}

	@Override
	public void startElement(String uri, String localName, String qName, Attributes att) {

		if(uri == "http://purl.org/rss/1.0/modules/slash/")
			compteur++;

	}

	@Override
	public void endElement(String uri, String localName, String qName) {
	}
}
