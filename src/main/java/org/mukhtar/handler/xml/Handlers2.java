package org.mukhtar.handler.xml;

import org.xml.sax.Attributes;
import org.xml.sax.helpers.DefaultHandler;

public class Handlers2 extends DefaultHandler {

	private String racine;
	private int depth = 0;
	private int compteur;

	@Override
	public void startDocument() {

	}

	@Override
	public void startElement(String uri, String localName, String qName, Attributes att) {
		if (depth == 0)
			racine = qName;

		if (depth == 1) // depth = 1 correspond à une sous racine
			compteur++;

		depth++;

	}

	@Override
	public void endElement(String uri, String localName, String qName) {

		depth--;

		/* Lorsqu'on a atteint la fin du fichier */
		if (qName.equals(racine))
			System.out.println("Il y a " + compteur + " sous racines dans le fichier XML");
	}
}
