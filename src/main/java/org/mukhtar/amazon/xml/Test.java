/* MUKHTAR Saqab */
package org.mukhtar.amazon.xml;

import java.io.IOException;
import java.io.InputStream;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.mukhtar.xml.RSSReader;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

/* --- Donne le nom de la racine du fichier XML --- */

public class Test extends DefaultHandler {

	public static void main(String[] args)
			throws ParserConfigurationException, SAXException, UnsupportedOperationException, IOException {

		/* --- Premier test --- */
		SAXParserFactory spf = SAXParserFactory.newInstance();
		spf.setValidating(true);
		spf.setNamespaceAware(true);
		InputStream is = RSSReader.read("http://docs.aws.amazon.com/AWSECommerceService/latest/DG/Welcome.html");
		Ex4_2 ex4_2 = new Ex4_2();
		SAXParser p = spf.newSAXParser();
		p.parse(is, ex4_2);

		System.out.print("Le nom de la racine est : ");
		System.out.println(ex4_2.getRootElementName());
		System.out.print("Le nom de l'URI par défaut est : ");
		System.out.println(Ex4_2.getElementURI());
		System.out.println("------------------------------------------------------------");

		/*
		 * is = RSSReader.read("https://www.voxxed.com/feed/"); Ex4_3 ex4_3 = new
		 * Ex4_3(); p.parse(is, ex4_3); ListIterator<String> iterateur =
		 * ex4_3.getListString().listIterator(); while(iterateur.hasNext())
		 * System.out.println(iterateur.next()); System.out.println(
		 * "------------------------------------------------------------");
		 */
	}
}
