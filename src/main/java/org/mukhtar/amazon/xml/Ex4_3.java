package org.mukhtar.amazon.xml;

import java.util.List;

import org.xml.sax.Attributes;
import org.xml.sax.helpers.DefaultHandler;

public class Ex4_3 extends DefaultHandler {

	private static String rootElementName;
	private static List<String> listString;;
	private int count = 0;

	public List<String> getListString() {
		return listString;
	}

	@Override
	public void startDocument() {
	}

	@Override
	public void startElement(String uri, String localName, String qName, Attributes att) {
		count++;
		if (count == 1)
			rootElementName = localName;
		listString.add(uri);
	}

	public String getRootElementName() {
		return rootElementName;
	}

}
