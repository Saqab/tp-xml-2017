package org.mukhtar.amazon.xml;

import org.xml.sax.Attributes;
import org.xml.sax.helpers.DefaultHandler;

public class Ex4_2 extends DefaultHandler {

	private static String rootElementName;
	private static String elementURI;
	private int count = 0;


	@Override
	public void startDocument() {
	}

	@Override
	public void startElement(String uri, String localName, String qName, Attributes att) {
		count++;
		if (count == 1)
			rootElementName = localName;
		elementURI = uri;
	}

	public static String getElementURI() {
		return elementURI;
	}

	public String getRootElementName() {
		return rootElementName;
	}

}
