package org.mukhtar.xml;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;

import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.dom4j.io.OutputFormat;
import org.dom4j.io.XMLWriter;

public class TestDoom4j {

	public static void main(String[] args) throws IOException {

		Document document = DocumentHelper.createDocument();
		Element rootElement = document.addElement("marin");
		rootElement.addAttribute("id", "12");
		rootElement.addElement("name").addText("Mukhtar");
		rootElement.addElement("prenom").addText("Saqab");
		rootElement.addElement("age").addText("25");

		OutputFormat format = OutputFormat.createPrettyPrint();
		Writer out = new FileWriter(new File("files/marin.xml"));
		XMLWriter writer = new XMLWriter(out, format);
		writer.write(document);
		writer.close();
	}
}
