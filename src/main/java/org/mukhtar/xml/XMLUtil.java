package org.mukhtar.xml;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.Iterator;

import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.dom4j.io.OutputFormat;
import org.dom4j.io.SAXReader;
import org.dom4j.io.XMLWriter;

public class XMLUtil {

	static Document serialize(Marin marin) {

		Document document = DocumentHelper.createDocument();
		Element rootElement = document.addElement("marin");
		rootElement.addAttribute("id", String.valueOf(marin.getId()));
		rootElement.addElement("name").addText(marin.getNom());
		rootElement.addElement("prenom").addText(marin.getPrenom());
		rootElement.addElement("age").addText(String.valueOf(marin.getAge()));

		return document;

	}

	static void write(Document doc, File file) throws IOException {
		OutputFormat format = OutputFormat.createPrettyPrint();
		Writer out = new FileWriter(file);
		XMLWriter writer = new XMLWriter(out, format);
		writer.write(doc);
		writer.close();
	}

	static Document read(File file) throws DocumentException {
		SAXReader xmlReader = new SAXReader();
		Document doc = xmlReader.read(file);
		return doc;
	}

	static Marin deserialize(Document doc) {

		Marin marin = new Marin(0, null, null, 0);
		Element root = doc.getRootElement();

		int age = 0;
		long id = 0;
		String nom = null;
		String prenom = null;

		id = Long.parseLong(root.attributeValue("id"));
		Iterator<Element> it = root.elementIterator();

		while (it.hasNext()) {
			Element elt = it.next();
			if (elt.getName().equals("age"))
				age = Integer.parseInt(elt.getText());
			if (elt.getName().equals("name"))
				nom = elt.getText();
			if (elt.getName().equals("prenom"))
				prenom = elt.getText();
		}

		marin.setAge(age);
		marin.setPrenom(prenom);
		marin.setNom(nom);
		marin.setId(id);

		return marin;
	}

	public static void main(String[] args) throws IOException, DocumentException {

		/* On arrive bien a cr�er un Marin sous XML */
		Marin marin = new Marin((long) 256, "Saqab", "Mukhtar", 25);
		Document marin_xml = serialize(marin);
		/*
		 * On arrive aussi � r�cup�rer le marin sous XML puis � le remettre sous
		 * XML
		 */
		File file = new File("files/XMLutil.xml");
		write(marin_xml, file);

		/*
		 * On teste la fonction deserialize - On v�rifie que l'objet Marin est
		 * bien initialis�
		 */
		Marin marin2 = deserialize(marin_xml);
		System.out.println(marin2.getAge());
		System.out.println(marin2.getNom());
		System.out.println(marin2.getPrenom());
		System.out.println(marin2.getId());

	}
}
