package org.mukhtar.xml;

import java.io.IOException;
import java.io.InputStream;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;

public class RSSReader {

	public static InputStream read(String url) throws UnsupportedOperationException, IOException {

		// ouverture d'un client
		HttpClient httpClient = HttpClientBuilder.create().build();

		// cr�ation d'une m�thode HTTP
		HttpGet get = new HttpGet(url);
		HttpResponse response = httpClient.execute(get);
		StatusLine statusline = response.getStatusLine();

		// invocation de la m�thode
		int statusCode = statusline.getStatusCode();
		if (statusCode != HttpStatus.SC_OK)
			System.err.println("Method failed : " + statusline);

		// lecture de la r�ponse dans un flux
		InputStream read = response.getEntity().getContent();
		return read;
	}

}
